import Vue from 'vue'
import App from './App'
import { BootstrapVue, BootstrapVuePlugin, BootstrapVueIcons } from "bootstrap-vue";
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/css/index.css'
Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
